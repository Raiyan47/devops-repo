curl -X POST http://127.0.0.1:8080/job/PipelineJobForEmail_Slack_Notification/build --user raiyanrashid:11d4dd42071198a1ab462090ee3bf07a87

curl -X GET http://localhost:8080/api/json?pretty=true --user raiyanrashid:11d4dd42071198a1ab462090ee3bf07a87

curl -X GET http://localhost:8080/api/json?pretty=true --user raiyanrashid:11d4dd42071198a1ab462090ee3bf07a87

curl -X GET http://localhost:8080/job/PipelineJobForEmail_Slack_Notification/lastSuccessfulBuild/api/json --user raiyanrashid:11d4dd42071198a1ab462090ee3bf07a87

curl -X GET http://localhost:8080/job/PipelineJobForEmail_Slack_Notification/lastBuild/consoleText/api/json --user raiyanrashid:11d4dd42071198a1ab462090ee3bf07a87


curl -X POST http://localhost:8080/job/job1/build --user jenkins:f1499cc9852c899d327a1f644e61a64d
You can also schedule the job start-up with some delay:

1
curl -X POST http://localhost:8080/job/job1/build?delay=10sec  --user jenkins:f1499cc9852c899d327a1f644e61a64d
What if you need to build a job with Parameters?

1
2
3
curl -X POST http://localhost:8080/job/job1/build  \
  -jenkins:f1499cc9852c899d327a1f644e61a64d \
  --data-urlencode json='{"parameter": [{"name":"id", "value":"100"}, {"name":"loglevel", "value":"high"}]}'
If you want to delete the job 'job1' then you can do it using the doDelete method:

1
curl -X POST http://localhost:8080/job/job1/doDelete --user jenkins:f1499cc9852c899d327a1f644e61a64d
On the other hand, if you want to delete in bulk some builds, you will add the build range before the doDelete method:

1
curl -X POST http://localhost:8080/job/job1/[1-100]/doDelete --user jenkins:f1499cc9852c899d327a1f644e61a64d
If you want a list of all jobs (with a nicely formatted JSON), then you can invoke the /api/json API with a simple GET request:

1
curl -X GET http://localhost:8080/api/json?pretty=true --user jenkins:f1499cc9852c899d327a1f644e61a64d



{
  "_class" : "hudson.model.Hudson",
  "assignedLabels" : [
    {
      "name" : "master"
    }
  ],
  "mode" : "NORMAL",
  "nodeDescription" : "the master Jenkins node",
  "nodeName" : "",
  "numExecutors" : 2,
  "description" : null,
  "jobs" : [
    {
      "_class" : "hudson.model.FreeStyleProject",
      "name" : "DEV",
      "url" : "http://172.16.18.128:8080/job/DEV/",
      "color" : "notbuilt"
    },
    {
      "_class" : "hudson.model.FreeStyleProject",
      "name" : "FirstJenkinsJob",
      "url" : "http://172.16.18.128:8080/job/FirstJenkinsJob/",
      "color" : "red"
    },
    {
      "_class" : "hudson.model.FreeStyleProject",
      "name" : "JenkinsOpsTree1",
      "url" : "http://172.16.18.128:8080/job/JenkinsOpsTree1/",
      "color" : "blue"
    },
    {
      "_class" : "hudson.model.FreeStyleProject",
      "name" : "JenkinsOpsTreeCloneJob",
      "url" : "http://172.16.18.128:8080/job/JenkinsOpsTreeCloneJob/",
      "color" : "red"
    },
    {
      "_class" : "hudson.model.FreeStyleProject",
      "name" : "JenkinsOpsTreeCompileJob",
      "url" : "http://172.16.18.128:8080/job/JenkinsOpsTreeCompileJob/",
      "color" : "blue"
    },
    {
      "_class" : "hudson.model.FreeStyleProject",
      "name" : "JenkinsOpsTreeDeployJob",
      "url" : "http://172.16.18.128:8080/job/JenkinsOpsTreeDeployJob/",
      "color" : "blue"
    },
    {
      "_class" : "hudson.model.FreeStyleProject",
      "name" : "JenkinsOpsTreePackageJob",
      "url" : "http://172.16.18.128:8080/job/JenkinsOpsTreePackageJob/",
      "color" : "blue"
    },
    {
      "_class" : "hudson.model.FreeStyleProject",
      "name" : "Master_slave_job",
      "url" : "http://172.16.18.128:8080/job/Master_slave_job/",
      "color" : "blue"
    },
    {
      "_class" : "org.jenkinsci.plugins.workflow.job.WorkflowJob",
      "name" : "PipelineJobForEmail_Slack_Notification",
      "url" : "http://172.16.18.128:8080/job/PipelineJobForEmail_Slack_Notification/",
      "color" : "blue"
    },
    {
      "_class" : "org.jenkinsci.plugins.workflow.job.WorkflowJob",
      "name" : "PipeLineSample",
      "url" : "http://172.16.18.128:8080/job/PipeLineSample/",
      "color" : "blue"
    },
    {
      "_class" : "hudson.model.FreeStyleProject",
      "name" : "PROD",
      "url" : "http://172.16.18.128:8080/job/PROD/",
      "color" : "notbuilt"
    },
    {
      "_class" : "hudson.model.FreeStyleProject",
      "name" : "PT",
      "url" : "http://172.16.18.128:8080/job/PT/",
      "color" : "notbuilt"
    },
    {
      "_class" : "hudson.model.FreeStyleProject",
      "name" : "QA",
      "url" : "http://172.16.18.128:8080/job/QA/",
      "color" : "notbuilt"
    },
    {
      "_class" : "hudson.model.FreeStyleProject",
      "name" : "UAT",
      "url" : "http://172.16.18.128:8080/job/UAT/",
      "color" : "notbuilt"
    }
  ],
  "overallLoad" : {
    
  },
  "primaryView" : {
    "_class" : "hudson.model.AllView",
    "name" : "all",
    "url" : "http://172.16.18.128:8080/"
  },
  "quietingDown" : false,
  "slaveAgentPort" : 0,
  "unlabeledLoad" : {
    "_class" : "jenkins.model.UnlabeledLoadStatistics"
  },
  "useCrumbs" : true,
  "useSecurity" : true,
  "views" : [
    {
      "_class" : "au.com.centrumsystems.hudson.plugin.buildpipeline.BuildPipelineView",
      "name" : "OpstreeSampleBuildingView",
      "url" : "http://172.16.18.128:8080/view/OpstreeSampleBuildingView/"
    },
    {
      "_class" : "hudson.model.ListView",
      "name" : "PROJECT1",
      "url" : "http://172.16.18.128:8080/view/PROJECT1/"
    },
    {
      "_class" : "hudson.model.ListView",
      "name" : "PROJECT2",
      "url" : "http://172.16.18.128:8080/view/PROJECT2/"
    },
    {
      "_class" : "hudson.model.ListView",
      "name" : "PROJECT3",
      "url" : "http://172.16.18.128:8080/view/PROJECT3/"
    },
    {
      "_class" : "hudson.model.AllView",
      "name" : "all",
      "url" : "http://172.16.18.128:8080/"
    }
  ]
}
